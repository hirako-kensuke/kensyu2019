<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消費税計算</title>
  </head>
  <body>
    <h1>消費税計算</h1>
     <form method="GET" action="">
      <table border="1" style="border-collapse:collapse;">

      <tr>
        <th align="rigth"><b>商品名</b></td>
        <th align="rigth"><b>価格（単価：円、税抜き）</b></td>
        <th align="rigth"><b>個数</b></td>
        <th align="rigth"><b>税率</b></td>
       </tr><br>
      <tr>
        <td><input type="text" name="p_namae01" ></td>
        <td><input type="number" name="nedan01" ></td>
        <td><input type="number" name="kazu01" >個</td>
        <td>
          <input type="radio" name="zeiritu01" value="8">8%
          <input type="radio" name="zeiritu01" value="10">10%
        </td>
       </tr><br>
       <tr>
         <td><input type="text" name="p_namae02" ></td>
         <td><input type="number" name="nedan02" ></td>
         <td><input type="number" name="kazu02" >個</td>
         <td>
           <input type="radio" name="zeiritu02" value="8">8%
           <input type="radio" name="zeiritu02" value="10">10%
         </td>
        </tr><br>
        <tr>
          <td><input type="text" name="p_namae03" ></td>
          <td><input type="number" name="nedan03" ></td>
          <td><input type="number" name="kazu03" >個</td>
          <td>
            <input type="radio" name="zeiritu03" value="8">8%
            <input type="radio" name="zeiritu03" value="10">10%
          </td>
         </tr><br>
         <tr>
           <td><input type="text" name="p_namae04" ></td>
           <td><input type="number" name="nedan04" ></td>
           <td><input type="number" name="kazu04" >個</td>
           <td>
             <input type="radio" name="zeiritu04" value="8">8%
             <input type="radio" name="zeiritu04" value="10">10%
           </td>
          </tr><br>
          <tr>
            <td><input type="text" name="p_namae05" ></td>
            <td><input type="number" name="nedan05" ></td>
            <td><input type="number" name="kazu05" >個</td>
            <td>
              <input type="radio" name="zeiritu05" value="8">8%
              <input type="radio" name="zeiritu05" value="10">10%
            </td>
           </tr><br>

      <tr>
         <td>
          <input type="submit" value="送信する">
          <input type="reset" value="リセットする">
        </td>
      </tr>

     </table>
    </form>
    <hr>

    <?php

    function blankCheck($formName){
      if(isset($_GET[$formName])) {
        return $_GET[$formName];
      } else {
        return "";
      }
    }

     ?>

    <table border="1">
     <tr>
       <th align="rigth"><b>商品名</b></td>
       <th align="rigth"><b>価格（単価：円、税抜き）</b></td>
       <th align="rigth"><b>個数</b></td>
       <th align="rigth"><b>税率</b></td>
       <th align="rigth"><b>小計（単位：円）</b></td>
     </tr>

     <tr>
      <td><?php echo blankCheck('p_namae01');?></td>
      <td><?php echo $_GET['nedan01'];?></td>
      <td><?php echo $_GET['kazu01']; ?></td>
      <td><?php echo $_GET['zeiritu01']."%";?></td>
      <td><?php
      $price1 = $_GET['nedan01'];
      $kazu1 = $_GET['kazu01'];
       if ($_GET['zeiritu01'] == "10") {
         $price1 = $price1 * 1.1;
       }else {
         $price1 = $price1 * 1.08;
       }
       $price1 = $price1 * $kazu1;
       echo number_format($price1) . "円（税込み）";
       ?></td>
      </tr>
      <tr>
       <td><?php echo $_GET['p_namae02'];?></td>
       <td><?php echo $_GET['nedan02'];?></td>
       <td><?php echo $_GET['kazu02']; ?></td>
       <td><?php echo $_GET['zeiritu02']."%";?></td>
       <td><?php
       $price2 = $_GET['nedan02'];
       $kazu2 = $_GET['kazu02'];
        if ($_GET['zeiritu02'] == "10") {
          $price2 = $price2 * 1.1;
        }else {
          $price2 = $price2 * 1.08;
        }
        $price2 = $price2 * $kazu2;
        echo number_format( $price2). "円（税込み）";
        ?></td>
       </tr>
       <tr>
        <td><?php echo $_GET['p_namae03'];?></td>
        <td><?php echo $_GET['nedan03'];?></td>
        <td><?php echo $_GET['kazu03']; ?></td>
        <td><?php echo $_GET['zeiritu03']."%";?></td>
        <td><?php
        $price3 = $_GET['nedan03'];
        $kazu3 = $_GET['kazu03'];
         if ($_GET['zeiritu03'] == "10") {
           $price3 = $price3 * 1.1;
         }else {
           $price3 = $price3 * 1.08;
         }
         $price3 = $price3 * $kazu3;
         echo number_format($price3). "円（税込み）";
         ?></td>
        </tr>
        <tr>
         <td><?php echo $_GET['p_namae04'];?></td>
         <td><?php echo $_GET['nedan04'];?></td>
         <td><?php echo $_GET['kazu04']; ?></td>
         <td><?php echo $_GET['zeiritu04']."%";?></td>
         <td><?php
         $price4 = $_GET['nedan04'];
         $kazu4 = $_GET['kazu04'];
          if ($_GET['zeiritu04'] == "10") {
            $price4 = $price4 * 1.1;
          }else {
            $price4 = $price4 * 1.08;
          }
          $price4 = $price4 * $kazu4;
          echo number_format($price4). "円（税込み）";
          ?></td>
         </tr>
         <tr>
          <td><?php echo $_GET['p_namae05'];?></td>
          <td><?php echo $_GET['nedan05'];?></td>
          <td><?php echo $_GET['kazu05']; ?></td>
          <td><?php echo $_GET['zeiritu05']."%";?></td>
          <td><?php
          $price5 = $_GET['nedan05'];
          $kazu5 = $_GET['kazu05'];
           if ($_GET['zeiritu05'] == "10") {
             $price5 = $price5 * 1.1;
           }else {
             $price5 = $price5 * 1.08;
           }
           $price5 = $price5 * $kazu5;
           echo number_format($price5). "円（税込み）";
           ?></td>
          </tr>

       <tr>
         <th align="rigth" colspan="4" style="text-align:left;"><b>合計</b></td>
         <th><?php
         $goukei = $price1 + $price2 + $price3 + $price4 +$price5;
         echo number_format($goukei). "円（税込み）";
          ?></td>
       </tr>
    </table>
  </body>
</html>
