
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、消費税計算</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>第二回課題、消費税計算</h1>
      <form method="post" action="tax.php">
        <!--
          ここにform部品を自由に配置してみよう
        --->
        <table border="1" style="border-collapse:collapse;">
          <thead>
            <tr><th>商品名</th><th>価格（単位：円、税抜き）</th><th>個数</th><th>税率</th></tr>
          </thead>
          <tbody>
            <tr><td><input type="text" name="p_name01"></td><td><input type="text" name="p_price01"></td><td><input type="text" size="2" name="p_num01">個</td><td><input type="radio" name="p_taxrate01" value="8" checked>0.8%<input type="radio" name="p_taxrate01" value="10">10%</td></tr>
            <tr><td><input type="text" name="p_name02"></td><td><input type="text" name="p_price02"></td><td><input type="text" size="2" name="p_num02">個</td><td><input type="radio" name="p_taxrate02" value="8" checked>0.8%<input type="radio" name="p_taxrate02" value="10">10%</td></tr>
            <tr><td><input type="text" name="p_name03"></td><td><input type="text" name="p_price03"></td><td><input type="text" size="2" name="p_num03">個</td><td><input type="radio" name="p_taxrate03" value="8" checked>0.8%<input type="radio" name="p_taxrate03" value="10">10%</td></tr>
            <tr><td><input type="text" name="p_name04"></td><td><input type="text" name="p_price04"></td><td><input type="text" size="2" name="p_num04">個</td><td><input type="radio" name="p_taxrate04" value="8" checked>0.8%<input type="radio" name="p_taxrate04" value="10">10%</td></tr>
            <tr><td><input type="text" name="p_name05"></td><td><input type="text" name="p_price05"></td><td><input type="text" size="2" name="p_num05">個</td><td><input type="radio" name="p_taxrate05" value="8" checked>0.8%<input type="radio" name="p_taxrate05" value="10">10%</td></tr>
          </tbody>
        </table>

        <input type="submit"><input type="reset">
      </form>

      <hr>


      <table border="1" style="border-collapse:collapse;">
        <thead>
          <tr><th>商品名</th><th>価格（単位：円、税抜き）</th><th>個数</th><th>税率</th><th>小計(単位：円)</tr>
        </thead>
        <tbody>
          <tr><td>りんご</td><td>80</td><td>20</td><td>0.08%</td><td>1728</td></tr>
          <tr><td>ホットケーキミックス</td><td>250</td><td>4</td><td>0.10%</td><td>1100</td></tr>
          <tr><td>手羽先</td><td>320</td><td>3</td><td>0.08%</td><td>1036</td></tr>
          <tr><td>ギョーザの皮</td><td>180</td><td>4</td><td>0.10%</td><td>792</td></tr>
          <tr><td>たけのこ水煮</td><td>200</td><td>3</td><td>0.08%</td><td>648</td></tr>
          <tr><td colspan="4">合計</td><td>5304</td></tr>
        </tbody>
      </table>

      <?php
       ?>
  </body>
</html>
