
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>第二回課題、フォーム部品練習</h1>
      <form method="get" action="table.php">
        <input type="text" name="rows" size="2">行 x <input type="text" name="cols" size="2">列<br/>
        <input type="submit"><input type="reset">
      </form>
      <hr>

      <table border="1">
        <tbody>
          <tr><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td></tr>
          <tr><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td></tr>
          <tr><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td></tr>
          <tr><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td></tr>
          <tr><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td><td>テストテスト</td></tr>
        </tbody>
      </table>

  </body>
</html>
