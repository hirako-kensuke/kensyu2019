
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>第二回課題、フォーム部品練習</h1>
      <form method="get" action="table.php">
        <input type="text" name="rows" size="2">行 x <input type="text" name="cols" size="2">列<br/>
        <input type="submit"><input type="reset">
      </form>
      <hr>

      <table border="1">
        <tbody>
          <tr><td>1-1</td><td>1-2</td><td>1-3</td><td>1-4</td><td>1-5</td></tr>
          <tr><td>2-1</td><td>2-2</td><td>2-3</td><td>2-4</td><td>2-5</td></tr>
          <tr><td>3-1</td><td>3-2</td><td>3-3</td><td>3-4</td><td>3-5</td></tr>
          <tr><td>4-1</td><td>4-2</td><td>4-3</td><td>4-4</td><td>4-5</td></tr>
          <tr><td>5-1</td><td>5-2</td><td>5-3</td><td>5-4</td><td>5-5</td></tr>
        </tbody>
      </table>

  </body>
</html>
