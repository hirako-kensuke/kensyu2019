<!DOCTYPE html>
<?php
  include("./include/statics.php");
  include("./include/functions.php");

  // パラメーターのチェック
  // パラメーター名はentry02.phpと共通なので注意
  $params = array("name01", "pref", "seibetu01", "age01", "section01", "grade01");
  if(!checkParams($params, 2)){
    // 不備があったらエラー。
    commonError();
  }

  $pdo = initDB();
  $query_str = "UPDATE member as m SET name = '"
              . $_POST['name01'] . "', m.pref = '" . $_POST['pref'] . "', m.seibetu = '"
              . $_POST['seibetu01'] . "', m.age = '" . $_POST['age01'] . "', m.section_ID = '"
              . $_POST['section01'] . "', m.grade_ID = '" . $_POST['grade01'] . "'
                WHERE m.member_ID = " . $_POST['member_ID'];

  // echo $query_str;
  try{
    $sql = $pdo->prepare($query_str);
    $sql->execute();
  }catch(PDOException $e){
    print $e->getMessage();
  }

  //header("'Location:http://127.0.0.1/work/04/detail01.php?member_ID=' . $_POST['member_ID'].");
  // $url = "http://192.168.13.217/work/sample/detail01.php?member_ID=" . $_POST['member_ID'];
  // $url = "http://192.168.11.183/work/sample/detail01.php?member_ID=" . $_POST['member_ID'];
  // $url = "http://127.0.0.1/work/04/detail01.php?member_ID=" . $_POST['member_ID'];
  // $url = "http://www.yahoo.co.jp";

  // $url = $stat_base_url . "detail01.php?member_ID=" . $_POST['member_ID'];
  $url = "detail01.php?member_ID=" . $_POST['member_ID'];

  header('Location: ' . $url);
  exit;
?>
