<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // common
  include("./include/functions.php");
  include("./include/statics.php");

  $temp_id = checkGetParam("member_ID");
  if(!$temp_id){
    commonError();
    // exit();
  }

  $pdo = initDB();
  $query_str = "SELECT m.member_ID, m.name, m.pref, m.seibetu, m.age, m.section_ID, m.grade_ID
                FROM member as m
                /*LEFT JOIN section1_master as sec ON m.section_ID = sec.ID
                LEFT JOIN grade_master as gr ON m.grade_ID = gr.ID*/
                WHERE m.member_ID = " . $temp_id;

   //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

  if(count($result) != 1){
    commonError();
  }

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>編集ページ</title>
    <script src="include/functions.js"></script>
  </head>
  <body>
  <?php include("./include/header.php"); ?>
    <hr>

    <form method="POST" action="update02.php" name="mainform">
      <div class="result_wrap detail_result" id="tbl-bdr">
        <table border="1" style="border-collapse:collapse;">
          <tr>
            <th>社員ID</th>
            <td>
              <?php echo $result[0]['member_ID'];?>
            </td>
          </tr>
          <tr>
            <th>名前</th>
            <td><input type="text" name="name01" value="<?php echo $result[0]['name'];?>"></td>
          </tr>
          <tr>
            <th>出身地</th>
            <td>
              <select name="pref">
                <?php
                 foreach($pref_array as $key => $value){
                   echo "<option value='" . $key . "'";
                   if($result[0]['pref'] == $key){ echo " selected ";}
                   echo ">" . $value . "</option>";
                 }
                 ?>
              </select>
            </td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
              <input id='sex01' type='radio' name='seibetu01' <?php if ($result[0]['seibetu'] == "1") { echo "checked"; } ?> value='1'><label for='sex01'>男</label>
              <input id='sex02' type='radio' name='seibetu01' <?php if ($result[0]['seibetu'] == "2") { echo "checked"; } ?> value='2'><label for='sex02'>女</label>
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input type="number" name="age01" value="<?php echo $result[0]['age'];?>" max="99" min="1" required></td>
          </tr>
          <tr>
            <th>所属部署</th>
            <td>
              <?php
                 foreach(getSection() as $each){
                   echo "<input type='radio' id='sec" . $each['ID']
                       . "' name='section01' value='" . $each['ID'] . "'";
                   if($each['ID'] == $result[0]['section_ID']) echo " checked ";
                   echo "><label for='sec" . $each['ID'] . "'>" . $each['section_name'] . "</label>";
                 }
               ?>
            </td>
          </tr>
          <tr>
            <th>役職</th>
            <td>
              <?php
                 foreach(getGrade() as $each){
                   echo "<input type='radio' id='grd" . $each['ID']
                        . "' name='grade01' value='" . $each['ID'] . "'";
                   if($each['ID'] == $result[0]['grade_ID']) echo " checked ";
                   echo "><label for='grd" . $each['ID'] . "'>" . $each['grade_name'] . "</label>";
                 }
             ?>
            </td>
          </tr>
        </table>
      </div>
      <div class="ta_rt">
        <input type="button" value="登録" onclick="conf();">
        <input type="reset" value="リセット">
        <input type="hidden" value="<?php echo $result[0]['member_ID'];?>" name="member_ID">
      </div>
    </form>
  </body>
</html>
