<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // common
  include("./include/functions.php");
  include("./include/statics.php");
  $pdo = initDB();
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員登録画面</title>
    <script src="include/functions.js"></script>
  </head>

  <body>
    <?php include("./include/header.php"); ?>
  <hr>
    <form method="post" action="entry02.php" name='mainform'>
      <div class="result_wrap detail_result" id="tbl-bdr">
        <table border="1" style="border-collapse:collapse;">
         <tr>
           <th>名前</th>
           <td><input type="text" name="name01"></td>
         </tr>
         <tr>
           <th>出身地</th>
           <td><select name="pref">
             <option value="" selected>都道府県</option>
             <?php
              foreach($pref_array as $key => $value){
                echo "<option value='" . $key . "'>" . $value . "</option>";
              }
              ?>
           </select> </td>
         </tr>
         <tr>
           <th>性別</th>
           <td>
              <?php // static.phpに配列は用意したけど面倒だから使ってないです。?>
              <input id='sex01' type="radio" name="seibetu01" checked value="1"><label for='sex01'>男</label>
              <input id='sex02' type="radio" name="seibetu01" value="2"><label for='sex02'>女</label>
           </td>
         </tr>
         <tr>
           <th>年齢</th>
           <td><input type="number" name="age01" max="99" min="1" required></td>
         </tr>
         <tr>
           <th>所属部署</th>
           <td>
             <?php
                foreach(getSection() as $each){
                  echo "<input type='radio' id='sec" . $each['ID']
                      . "' name='section01' value='" . $each['ID'] . "'";
                  if($each['ID'] == "1") echo " checked ";
                  echo "><label for='sec" . $each['ID'] . "'>" . $each['section_name'] . "</label>";
                }
                ?>
           </td>
         </tr>
         <tr>
           <th>役職</th>
           <td>
             <?php
                foreach(getGrade() as $each){
                  echo "<input type='radio' id='grd" . $each['ID']
                       . "' name='grade01' value='" . $each['ID'] . "'";
                  if($each['ID'] == "1") echo " checked ";
                  echo "><label for='grd" . $each['ID'] . "'>" . $each['grade_name'] . "</label>";
                }
                ?>
           </td>
         </tr>
         </table>
      </div>
      <div class="ta_rt">
        <input type="button" value="登録" onclick="conf();">
        <input type="reset" value="リセット">
      </div>
    </form>
  </body>
</html>
