<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php

if (is_uploaded_file($_FILES["csvfile"]["tmp_name"])) {
  $file_tmp_name = $_FILES["csvfile"]["tmp_name"];
  $file_name = $_FILES["csvfile"]["name"];

  //拡張子を判定
  if (pathinfo($file_name, PATHINFO_EXTENSION) != 'csv') {
    $err_msg = 'CSVファイルのみ対応しています。';
  } else {
    //ファイルをdataディレクトリに移動
    if (move_uploaded_file($file_tmp_name, "/tmp/" . $file_name)) {
      //後で削除できるように権限を644に
      chmod("/tmp/" . $file_name, 0644);
      $msg = $file_name . "をアップロードしました。";
      $file = '/tmp/'.$file_name;
      $fp   = fopen($file, "r");

      //配列に変換する
      while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
        $asins[] = $data;
        // $asins[] = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
      }
      fclose($fp);
      //ファイルの削除
      unlink('/tmp/'.$file_name);
    } else {
      $err_msg = "ファイルをアップロードできません。";
    }
  }
} else {
  $err_msg = "ファイルが選択されていません。";
}

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>csvインポート</title>
  </head>

  <body>
    <?php include("./include/header.php"); ?>
  <hr>
  <pre>
    <?php var_dump($asins); ?>
  </pre>

  <table>
    <?php
      foreach($asins as $each){
          echo "<tr>";
            foreach($each as $data){
              echo "<td>" . mb_convert_encoding($data, 'UTF-8', 'sjis-win') . "</td>";
            }
          echo "</tr>";
      }
     ?>
   </table>

   <?php

       // common
       include("./include/functions.php");
       $pdo = initDB();

      $array_fields = $asins[0]; // フィールド名の配列作成
      $query_str_base = "INSERT INTO member(";
      foreach($array_fields as $each){
          $query_str_base .= $each . ",";
      }
      $query_str_base = substr($query_str_base, 0, -1); // 最後の1文字削除
      $query_str_base .= ") VALUES (";


      array_splice($asins, 0, 1); // データ配列からフィールド名行を削除する
      echo "<hr>削除後の配列";
      var_dump($asins);
      echo "<hr/>";
      foreach($asins as $each){
          $query_str = $query_str_base;
          foreach($each as $data){
            // $query_str .= "'" . $data . "',";
            $query_str .= "'" . mb_convert_encoding($data, 'UTF-8', 'sjis-win') . "',";
          }
          $query_str = substr($query_str, 0, -1); // 最後の1文字削除
          $query_str .= ")";
          echo $query_str . "<hr/>";

          // echo $query_str;
          try{
            $sql = $pdo->prepare($query_str);
            $sql->execute();

            $id = $pdo->lastInsertId('member_ID');

          }catch(PDOException $e){
            print $e->getMessage();
          }

      }
    ?>
   <a href="index_csv.php">back to csv importer</a>
  </body>
</html>
