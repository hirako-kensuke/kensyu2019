function conf(){

  //名前からスペースの除去、それから入力チェック
  var temp_name = document.mainform.name01.value;
  temp_name =  temp_name.replace(/\s+/g,"");
  document.mainform.name01.value = temp_name;
  if( temp_name == ""){
    alert("名前は必須です");
    return false;
  }

  //年齢上限下限チェック
  var temp_age = document.mainform.age01.value;
  // alert(temp_age);
  if( temp_age == "" || Number.isInteger(temp_age)
      || temp_age < 1 || temp_age > 100){
    alert("年齢は必須です/数値を入力してください/1-99の範囲で入力してください");
    return false;
  }

  if(document.mainform.pref.value == ""){
    alert("都道府県は必須です");
    return false;
  }

  if(window.confirm('更新を行います。よろしいですか？')){
    document.mainform.submit();
    // alert("true");
  } else {
    // alert("false");
  //
  }
}
