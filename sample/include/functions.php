<?php


function initDB(){
  include("./include/statics.php");
  // $DB_USER = "webaccess";
  // $DB_PW = "LiIUIF7SGNum2Hhe";
  $DB_DSN = "mysql:host=localhost;dbname=test;charset=utf8";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  return $pdo;
}

function getGrade(){
  $pdo = initDB();
  $query_str = "select * from grade_master order by id";
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  return $sql->fetchAll();
}

function getSection(){
  $pdo = initDB();
  $query_str = "select * from section1_master order by id";
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  return $sql->fetchAll();
}

function checkGetParam($key){
  if(isset($_GET[$key]) AND $_GET[$key] != ""){
    return $_GET[$key];
  } else {
    return NULL;
  }
}

function checkPostParam($key){
  if(isset($_POST[$key]) AND $_POST[$key] != ""){
    return $_POST[$key];
  } else {
    return false;
  }
}

// 複数のフォームパラメーターを一気にチェックする関数
// $method ... 1: GET, 2: POST
function checkParams($keys, $method){
  // echo "you've got checkParams<br/>";
  if($method == "1"){
    // echo "get<br/>";
    foreach($keys as $each){
      if(!checkGetParam($each)){
        return false;
      }
    }
    return true;
  }elseif($method == "2"){
    // echo "post</br>";
    foreach($keys as $each){
      // echo $each . "<br/>";
      if(!checkPostParam($each)){
        return false;
      }
    }
    return true;
  }else{
    echo "method error.";
    return false;
  }
}


function commonError(){
  echo "エラーが発生しました。<br/>";
  echo "<a href='./index.php'>トップページに戻る</a>";
  exit();
}


?>
