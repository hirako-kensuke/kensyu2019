<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // common
  include("./include/functions.php");
  include("./include/statics.php");
  $pdo = initDB();
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員一覧画面</title>
    <script type="text/javascript">
      <!--
      // スペースのサニタイズ
      function delSpace(){
        var temp = document.searchForm.namae.value;
        document.searchForm.namae.value = temp.replace(/\s+/g,"");
      }

      // フォームリセット
      function clearForm(){
        document.searchForm.namae.value = "";
        document.searchForm.sex.value = "0";
        document.searchForm.section.value = "0";
        document.searchForm.grade.value = "0";
      }
          -->
    </script>

  </head>
  <body>
    <?php include("./include/header.php"); ?>
  <hr>
  <form method="GET" action="./index_csv.php" name='searchForm'>
    <div class="search_form">
    <b>名前：</b><input type="search" name="namae" value="<?= checkGetParam("namae"); ?>">
    <br/>
    <b>性別：</b>
    <select name="sex">
      <option value= "0" >すべて</option>
        <option value= "1" <?php echo checkGetParam('sex') == "1" ? "selected" : ""; ?>>男</option>
        <option value= "2" <?php echo checkGetParam('sex') == "2" ? "selected" : ""; ?>>女</option>
    </select>
    <b>部署：</b>
    <select name="section">
      <option value= "0" >すべて</option>
      <?php
        $result = getSection();
        foreach($result as $each){
          if($_GET['section'] == $each['ID']){
            echo "<option value= '" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
          } else {
            echo "<option value= '" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
          }
        }
       ?>
    </select>
    <b>役職：</b>
    <select name="grade">
      <option value= "0" >すべて</option>
      <?php
        $result = getGrade();
        foreach($result as $each){
          if($_GET['grade'] == $each['ID']){
            echo "<option value= '" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
          }else{
            echo "<option value= '" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
          }
        }
       ?>
    </select>
  </div>
  <div class="ta_c">
      <input type="submit" value="検索" onClick="delSpace();">
      <input type="button" value="リセット" onClick='clearForm();'>
  </div>
  </form>
    <hr>

    <?php


    $query_str = "SELECT m.member_ID, m.name, sc.section_name, gr.grade_name, m.seibetu, m.pref, m.age
                   FROM member as m
                   LEFT JOIN grade_master as gr ON m.grade_ID = gr.ID
                   LEFT JOIN section1_master as sc ON m.section_ID = sc.ID
                   WHERE 1=1 ";


    if(checkGetParam('namae')){
        $query_str .= " AND m.name LIKE '%" . checkGetParam('namae') . "%' ";
    }

    if(checkGetParam('sex')){
        $query_str .= " AND m.seibetu ='" . checkGetParam('sex') . "' ";
    }

    if(checkGetParam('section')){
        $query_str .= " AND m.section_ID ='" . checkGetParam('section') . "' ";
    }

    if(checkGetParam('grade')){
        $query_str .= " AND m.grade_ID ='" . checkGetParam('grade') . "' ";
    }

    // echo $query_str;
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();


    if(array_key_exists('csv', $_GET) && $_GET['csv'] == "1"){
      // echo "yo yo yo.";
      try {
        //CSV形式で情報をファイルに出力のための準備
        $csvFileName = '/tmp/' . time() . rand() . '.csv';
        $res = fopen($csvFileName, 'w');
        if ($res === FALSE) {
          throw new Exception('ファイルの書き込みに失敗しました。');
        }

        // データ一覧。この部分を引数とか動的に渡すようにしましょう
        $dataList = array(
                array('社員番号', '名前', '年齢', '性別', '出身', '役職', '部署')
        );

        // ここで検索結果を配列にPushしていく
        foreach($result as $row){
          $temp_array = array(........);
          array_push($dataList, $temp_array);
        }
        // ループしながら出力
        foreach($dataList as $dataInfo) {
          // 文字コード変換。エクセルで開けるようにする
          mb_convert_variables('SJIS', 'UTF-8', $dataInfo);

          // ファイルに書き出しをする
          fputcsv($res, $dataInfo);
        }

        // ハンドル閉じる
        fclose($res);

        // ダウンロード修正版。参考：https://teratail.com/questions/40474
        header('Content-Description: File Transfer');
        header("Content-Disposition: attachment; filename=workerlist" . date(YmdHis) . ".csv");
        header('Content-Type: application/force-download;');

        ob_clean();
        flush();

        readfile($csvFileName);
        exit;


      } catch(Exception $e) {
        // 例外処理をここに書きます
        echo $e->getMessage();
      }
    }

     ?>

     <!-- <pre>
       <?php // var_dump($result); ?>
     </pre> -->
     <div class="result_wrap" id="tbl-bdr">
       検索結果：<?php echo count($result); ?> <br/>
       <table>
         <tr>
           <th>社員ID</th>
           <th>名前</th>
           <th>部署</th>
           <th>役職</th>
         </tr>
         <?php  //結果を出力する
         if(count($result) == 0){
            echo "<tr><td colspan='4' style='text-align: center;'>検索結果なし</td></tr>";
         }else{
           foreach($result as $each){
             echo "<tr><td>" . $each['member_ID'] . "</td>
                       <td><a href='detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>
                       <td>" . $each['section_name'] . "</td>
                       <td>" . $each['grade_name'] . "</td></tr>";
           }
         }
          ?>
       </table>
     </div> <!--- .result_wrap --->
     <form method="get" action="">
       <input type='hidden' name='csv' value='1'>
       <input type="submit" class="btn btn-success" onClick="generatecsv();" value="csvエクスポート">
     </form>
     <form method="post" action="importcsv.php" enctype="multipart/form-data">
       <input type="file" name="csvfile">
       <input type="submit" class="btn btn-success" onClick="generatecsv();" value="csvインポート">
     </form>
  </body>
</html>
