<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // common
  // include("./include/functions.php");
  $DB_DSN = "mysql:host=localhost; dbname=test; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "LiIUIF7SGNum2Hhe";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo = initDB();

  // 条件文を作成する。
  // 検索条件があるときのみWHERE文を作成する。
  $where_str = "";
  $cond_dishname = "";
  $cond_genre = "";
  $cond_pricefrom = "";
  $cond_priceto = "";
  if (isset($_GET['dish_name']) && !empty($_GET['dish_name'])) {
    $where_str .= " dish_name LIKE '%" . $_GET['dish_name'] . "%' AND";
    $cond_dishname = $_GET['dish_name'];
  }
  if (isset($_GET['genre']) && !empty($_GET['genre'])) {
    $where_str .= " genre = '" . $_GET['genre'] . "' AND";
    $cond_genre = $_GET['genre'];
  }
  if (isset($_GET['price_from']) && !empty($_GET['price_from'])) {
    $where_str .= " price >= '" . $_GET['price_from'] . "' AND";
    $cond_pricefrom = $_GET['price_from'];
  }
  if (isset($_GET['price_to']) && !empty($_GET['price_to'])) {
    $where_str .= " price <= '" . $_GET['price_to'] . "' AND";
    $cond_priceto = $_GET['price_to'];
  }

  // Where文以降を生成する。
  if($where_str != ""){
    // WHEREを頭につけて、語尾の4文字を削除する。
    $where_str = " WHERE " . substr($where_str, 0, -4);
  }

  // SQLの基本部分。全権検索のときはこれだけ。
  $query_str = "SELECT * FROM test_table";

  // 基本部分にwhere文をくっつける。条件が無ければ$where_strは空白だからそのままつけちゃう。
  $query_str .= $where_str;

  // $query_str = "SELECT * FROM `test_table` WHERE dish_name LIKE '%の%' AND genre = 'おつまみ'";

  echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>居酒屋ウェブレッジ水道橋店</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script type="text/javascript">
    <!--
    function resetForms(){
      // alert("hogehoge");
      // 名前の空欄判定
      document.dish_search.dish_name.value = "";
      document.dish_search.genre.value = "";
      document.dish_search.price_from.value = "";
      document.dish_search.price_to.value = "";
    }
    -->
    </script>
  </head>
  <body>

    <h1>居酒屋ウェブレッジ水道橋店</h1>
    <form method="get" action="" name="dish_search">
      <table class="table table-dark">
        <tr>
          <th>名前</th>
          <td><input type="text" name="dish_name" value="<?php echo $cond_dishname; ?>"></td>
        </tr>
        <tr>
          <th>ジャンル</th>
          <td>
            <select name="genre">
              <option value="">選択してください</option>
              <option value="おつまみ" <?php if($cond_genre == "おつまみ"){ echo "selected"; } ?>>おつまみ</option>
              <option value="お食事" <?php if($cond_genre == "お食事"){ echo "selected"; } ?>>お食事</option>
              <option value="海鮮" <?php if($cond_genre == "海鮮"){ echo "selected"; } ?>>海鮮</option>
            </select>
          </td>
        </tr>
        <tr>
          <th>値段</th>
          <td><input type="text" name="price_from" value="<?php echo $cond_pricefrom; ?>">～
              <input type="text" name="price_to" value="<?php echo $cond_priceto; ?>">円
          </td>
        </tr>
      </table>
      <input type="submit">
      <input type="button" value="リセット" onClick="resetForms();">
    </form>
    <hr>



    <!--
    <pre>
      <?php
      var_dump($result);
       ?>
    </pre> -->

    <table class="table table-striped">
      <thead class="thead-dark">
        <tr><th>料理名</th><th>値段</th><th>ジャンル</th><th>メモ</th></tr>
      </thead>
    <?php
    foreach($result as $each){
      // var_dump($each);
      echo "<tr>";
      echo "<td>" . $each['dish_name'] . "</td><td>" . number_format($each['price']) . " 円</td><td>" . $each['genre'] . "</td><td>" . $each['memo'] . "</td>" ;
      echo "</tr>";
    }
    ?>

  </body>
</html>
