<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php

// ファイルアップロードがあるかどうか判定
if (is_uploaded_file($_FILES["jsonfile"]["tmp_name"])) {
  $file_tmp_name = $_FILES["jsonfile"]["tmp_name"];
  $file_name = $_FILES["jsonfile"]["name"];

  //拡張子を判定
  if (pathinfo($file_name, PATHINFO_EXTENSION) != 'json') {
    $err_msg = 'jsonファイルのみ対応しています。';
  } else {
    //ファイルをdataディレクトリに移動
    if (move_uploaded_file($file_tmp_name, "/tmp/" . $file_name)) {
      //後で削除できるように権限を644に
      chmod("/tmp/" . $file_name, 0644);
      $msg = $file_name . "をアップロードしました。";
      $file = '/tmp/'.$file_name;

      // jsonファイルをレンダリング
      $temp_json_str = file_get_contents($file);

      // 第二引数をtrueにすると連想配列にしてくれる
      $temp_json = json_decode($temp_json_str, true);

      //ファイルの削除
      unlink('/tmp/'.$file_name);
    } else {
      $err_msg = "ファイルをアップロードできません。";
    }
  }
} else {
  $err_msg = "ファイルが選択されていません。";
}

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jsonインポート</title>
  </head>

  <body>
    <?php include("./include/header.php"); ?>
  <hr>
  <pre><!--- デバッグプリント。レンダリングしたjsonファイルをダンプ。 --->
    <?php var_dump($temp_json); ?>
  </pre>


   <?php
       // common
       include("./include/functions.php");
       $pdo = initDB();

       // １レコードずつ読み込んでSQLを作成する
       foreach($temp_json as $each){

          //
          // このへんにSQLを作成する処理をかいてみよう！！
          //


          // SQLを表示
          echo $query_str . "<hr/>";

          try{
            $sql = $pdo->prepare($query_str);
            $sql->execute();

            $id = $pdo->lastInsertId('member_ID');

          }catch(PDOException $e){
            print $e->getMessage();
          }
      }
    ?>
   <a href="index_json.php">back to json importer</a>
  </body>
</html>
