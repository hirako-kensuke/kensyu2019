<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>詳細ページ</title>
  </head>
  <script type="text/javascript">
  <!--
  function goDel(id){
    if(window.confirm('削除を行います。よろしいですか？')){
      location.href = "./delete01.php?member_ID=" + id;
      // document.update.submit();
      // alert("true");
    }
  }

  function goEdit(id){
    location.href = "./entry_update01.php?member_ID=" + id;
  }
  -->
  </script>

  <body>
    <?php include("./include/header.php"); ?>
  <hr>
  <?php
    // common
    include("./include/functions.php");
    include("./include/statics.php");

    $temp_id = checkGetParam("member_ID");

    if(!$temp_id){
      commonError();
      // exit();
    }

    $pdo = initDB();
    // $query_str = "SELECT m.member_ID, m.name, m.pref, m.seibetu, m.age, sec.section_name, gr.grade_name
    //     FROM member as m
    //     LEFT JOIN section1_master as sec ON m.section_ID = sec.ID
    //     LEFT JOIN grade_master as gr ON m.grade_ID = gr.ID
    //     WHERE m.member_ID ='". $_GET['member_ID']."'";
    $query_str = "SELECT m.member_ID, m.name, m.pref, m.seibetu, m.age, sec.section_name, gr.grade_name
        FROM member as m
        LEFT JOIN section1_master as sec ON m.section_ID = sec.ID
        LEFT JOIN grade_master as gr ON m.grade_ID = gr.ID
        WHERE m.member_ID = ". $temp_id;

    // echo $query_str;

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();

    if(count($result) != 1){
      commonError();
    }

    ?>
    <div class="result_wrap detail_result" id="tbl-bdr">
      <table>
        <tr>
          <th>社員ID</th>
          <td><?php echo $result[0]['member_ID'];?></td>
        </tr>
        <tr>
          <th>名前</th>
          <td><?php echo $result[0]['name'];?></td>
        </tr>
        <tr>
          <th>出身地</th>
          <td><?php echo $pref_array[$result[0]['pref']];?></td>
        </tr>
        <tr>
          <th>性別</th>
          <td><?php echo $gender_array[$result[0]['seibetu']];?></td>
        </tr>
        <tr>
          <th>年齢</th>
          <td><?php echo $result[0]['age'];?></td>
        </tr>
        <tr>
          <th>所属部署</th>
          <td><?php echo $result[0]['section_name'];?></td>
        </tr>
        <tr>
          <th><b>役職</th>
          <td><?php echo $result[0]['grade_name'];?></td>
        </tr>
      </table>
    </div>
    <div class="ta_rt">
      <input type="button" value="編集" onclick="goEdit(<?php echo $result[0]['member_ID']; ?>);">
      <input type="button" value="削除" onclick="goDel(<?php echo $result[0]['member_ID']; ?>);">
    </div>
  </body>
</html>
