<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // common
  include("./include/functions.php");
  $pdo = initDB();
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員一覧画面</title>
    <script type="text/javascript">
      <!--
      // スペースのサニタイズ
      function delSpace(){
        var temp = document.searchForm.namae.value;
        document.searchForm.namae.value = temp.replace(/\s+/g,"");
      }

      // フォームリセット
      function clearForm(){
        document.searchForm.namae.value = "";
        document.searchForm.sex.value = "0";
        document.searchForm.section.value = "0";
        document.searchForm.grade.value = "0";
      }
          -->
    </script>

  </head>
  <body>
    <?php include("./include/header.php"); ?>
  <hr>
  <form method="GET" action="./index.php" name='searchForm'>
    <div class="search_form">
    <b>名前：</b><input type="search" name="namae" value="<?= checkGetParam("namae"); ?>">
    <br/>
    <b>性別：</b>
    <select name="sex">
      <option value= "0" >すべて</option>
        <option value= "1" <?php echo checkGetParam('sex') == "1" ? "selected" : ""; ?>>男</option>
        <option value= "2" <?php echo checkGetParam('sex') == "2" ? "selected" : ""; ?>>女</option>
    </select>
    <b>部署：</b>
    <select name="section">
      <option value= "0" >すべて</option>
      <?php
        $result = getSection();
        foreach($result as $each){
          if($_GET['section'] == $each['ID']){
            echo "<option value= '" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
          } else {
            echo "<option value= '" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
          }
        }
       ?>
    </select>
    <b>役職：</b>
    <select name="grade">
      <option value= "0" >すべて</option>
      <?php
        $result = getGrade();
        foreach($result as $each){
          if($_GET['grade'] == $each['ID']){
            echo "<option value= '" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
          }else{
            echo "<option value= '" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
          }
        }
       ?>
    </select>
  </div>
  <div class="ta_c">
      <input type="submit" value="検索" onClick="delSpace();">
      <input type="button" value="リセット" onClick='clearForm();'>
  </div>
  </form>
    <hr>

    <?php


    $query_str = "SELECT m.member_ID, m.name, sc.section_name, gr.grade_name
                   FROM member as m
                   LEFT JOIN grade_master as gr ON m.grade_ID = gr.ID
                   LEFT JOIN section1_master as sc ON m.section_ID = sc.ID
                   WHERE 1=1 ";


    if(checkGetParam('namae')){
        $query_str .= " AND m.name LIKE '%" . checkGetParam('namae') . "%' ";
    }

    if(checkGetParam('sex')){
        $query_str .= " AND m.seibetu ='" . checkGetParam('sex') . "' ";
    }

    if(checkGetParam('section')){
        $query_str .= " AND m.section_ID ='" . checkGetParam('section') . "' ";
    }

    if(checkGetParam('grade')){
        $query_str .= " AND m.grade_ID ='" . checkGetParam('grade') . "' ";
    }

     echo $query_str;
     $sql = $pdo->prepare($query_str);
     $sql->execute();
     $result = $sql->fetchAll();
     ?>

     <!-- <pre>
       <?php // var_dump($result); ?>
     </pre> -->
     <div class="result_wrap" id="tbl-bdr">
       検索結果：<?php echo count($result); ?> <br/>
       <table>
         <tr>
           <th>社員ID</th>
           <th>名前</th>
           <th>部署</th>
           <th>役職</th>
         </tr>
         <?php  //結果を出力する
         if(count($result) == 0){
            echo "<tr><td colspan='4' style='text-align: center;'>検索結果なし</td></tr>";
         }else{
           foreach($result as $each){
             echo "<tr><td>" . $each['member_ID'] . "</td>
                       <td><a href='detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>
                       <td>" . $each['section_name'] . "</td>
                       <td>" . $each['grade_name'] . "</td></tr>";
           }
         }
          ?>
       </table>
     </div> <!--- .result_wrap --->
  </body>
</html>
